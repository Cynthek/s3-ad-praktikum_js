package paket2.eisenbahnzug;

/**
 * Exemplare dieser Klasse repr�sentieren einen Zug. Ein Zug beginnt mit einer 
 * Lokomotive, an die weitere Personenwagen oder Lokomotiven angeh�ngt werden 
 * k�nnen.
 * @author Julia-Marie
 *
 */
import java.util.ArrayList;

public class Zug 
{
	private Lokomotive _lokomotive;
	private ArrayList<AbstractWagon> _gesamterZug;

	/**
	 * Initialisiert einen Zug, der von der �bergebenen Lokomotive gezogen wird.
	 * 
	 * @param lokomotive cDie f�rende Lokomotive
	 */
	public Zug(Lokomotive lokomotive)
	{
		_lokomotive = lokomotive;
		_gesamterZug = new ArrayList<AbstractWagon>();
		_gesamterZug.add(_lokomotive);
	}

	/**
	 * H�ngt einen Personenwagen oder eine Lokomotive an den Zug an.
	 * 
	 * @param wagon Der anzuh�ngende Wagon
	 * @require wagon!=null
	 */
	public void haengeAn(AbstractWagon wagon)
	{
		assert wagon != null : "Vorbedingung verletzt: wagon != null";
		_gesamterZug.add(wagon);
	}

	/**
	 * Gibt einen String der aneinanderh�ngenden Beschreibungen aller Wagons
	 * zur�ck.
	 * 
	 * @return Eine Beschreibung aller Wagons.
	 */
	public String gibZugbeschreibung() 
	{
		String antwort = "";
		for (int i = 0; i < _gesamterZug.size(); i++) {
			antwort = antwort + " - " + _gesamterZug.get(i).toString();
		}
		return antwort;
	}

	/**
	 * Gibt die Laenge eines Zuges zur�ck.
	 * @return Die L�nge des Zuges.
	 */
	public int gibLaenge()
	{
		return _gesamterZug.size();
	}
}