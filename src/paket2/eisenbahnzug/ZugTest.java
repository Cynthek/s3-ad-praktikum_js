package paket2.eisenbahnzug;
/**
 * @author Julia Stache, Chris Bargmann
 */
import static org.junit.Assert.*;

import org.junit.Test;

public class ZugTest
{

	private Zug _zug;
	private Lokomotive _lokomotive;

	/**
	 * Pr�t, ob an einen Zug beliebige Wagons anhaengbar sind.
	 */
	@Test
	public void haengeAn()
	{
		_lokomotive = new Lokomotive("Zug", 15);

		_zug = new Zug(_lokomotive);
		_zug.haengeAn(new Personenwagen("Wagon1", 10));
		_zug.haengeAn(new Personenwagen("Wagon2", 10));
		_zug.haengeAn(new Personenwagen("Wagon3", 10));

		assertTrue(_zug.gibLaenge() == 4);

	}
}
