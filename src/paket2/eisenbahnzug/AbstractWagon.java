package paket2.eisenbahnzug;
/**
 * Exemplare dieser Klasse repräsentieren Wagons eines Zugs. 
 * @author Julia Stache, Chris Bargmann
 *
 */
abstract class AbstractWagon
{
	protected String _bezeichner;
	protected AbstractWagon _nachfolger;
	
	/**
	 * Initialisiert einen Wagon mit einem Bezeichner.
	 * @param bezeichner Der Bezeichner des Wagons.
	 * @require !bezeichner.equals("")
	 */
	public AbstractWagon(String bezeichner)
	{
		assert !bezeichner.equals("") : "Vorbedingung verletzt: String darf nicht leer sein";
		_bezeichner= bezeichner;
	}
}

