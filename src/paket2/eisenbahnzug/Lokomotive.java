package paket2.eisenbahnzug;
/**
 * Exemplare dieser Klasse repräsentieren eine Lokomotive.
 * @author Julia Stache, Chris Bargmann
 *
 */
public class Lokomotive extends AbstractWagon
{
	private int _zugkraft;
	
	/**
	 * Initialisiert eine Lokomotive mit einem Bezeichner und einer Zugkraft.
	 */
	public Lokomotive(String bezeichner, int zugkraft)
	{
		super(bezeichner);
		_zugkraft= zugkraft;
	}
	/**
	 * Gibt eine Beschreibung der Lokomotive als String zurück.
	 */
	@Override
	public String toString()
	{
		return "Name: " + _bezeichner + ", Zugkraft: "+ _zugkraft;
	}
	

	

}
