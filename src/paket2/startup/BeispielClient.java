package paket2.startup;

import paket2.eisenbahnzug.Lokomotive;
import paket2.eisenbahnzug.Personenwagen;
import paket2.eisenbahnzug.Zug;
/**
 * 
 * @author Julia Stache, Chris Bargmann
 * Diese Klasse erstellt einen Beispielzug und gibt seine Beschreibung auf der
 * Konsole aus.
 *
 */
public class BeispielClient 
{

	public static void main(String[] args)
	{
		Zug zug = erstelleZug();
		System.out.println(zug.gibZugbeschreibung());
		
	}
	private static Zug erstelleZug()
	{
		Zug zug = new Zug(new Lokomotive("Roland", 10));
		for (int i=1; i<11; i++)
		{
			zug.haengeAn(new Personenwagen("Waggon " + i , i*12));
		}
		return zug;
	}

}
