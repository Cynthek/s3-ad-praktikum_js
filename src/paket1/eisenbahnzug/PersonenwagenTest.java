package paket1.eisenbahnzug;

import static org.junit.Assert.*;

import org.junit.Test;

public class PersonenwagenTest
{

	@Test
	public void testAnhaengen()
	{
		Personenwagen testWagon = new Personenwagen("ICE",400);
		Personenwagen nachfolger = new Personenwagen ("Metronom",300);
		testWagon.anhaengen(nachfolger);
		assertEquals(testWagon.getNachfolger(), nachfolger);
	}

	@Test
	public void testIstLetzterWagen() 
	{
		Personenwagen testWagon = new Personenwagen("ICE",400);
		assertEquals(testWagon.getNachfolger(), null);
		assertEquals(testWagon.istLetzterWagen(), true);
		
		Personenwagen nachfolger = new Personenwagen ("Metronom",300);
		testWagon.anhaengen(nachfolger);
		assertEquals(testWagon.getNachfolger(), nachfolger);
		assertEquals(testWagon.istLetzterWagen(), false);
	}



}
