package paket1.eisenbahnzug;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
/**
 * 
 * @author Chris Bargmann, Julia Stache
 *
 */

public class ZugTest {

	private Zug _zug;
	private Lokomotive _lokomotive;

	/**
	 * Pr�ft, ob an einen Zug beliebige Wagons anhaengbar sind.
	 */
	@Test
	public void haengeAn() 
	{
		_lokomotive = new Lokomotive("Zug", 15);

		_zug = new Zug(_lokomotive);
		_zug.haengeAn(new Personenwagen("Wagon1", 10));
		_zug.haengeAn(new Personenwagen("Wagon2", 10));
		_zug.haengeAn(new Personenwagen("Wagon3", 10));

		assertTrue(_zug.gibLaenge(_lokomotive) == 4);

	}
}
