package paket1.eisenbahnzug;

import static org.junit.Assert.*;

import org.junit.Test;

public class LokomotiveTest {
	@Test
	public void testAnhaengen()
	{
		Lokomotive testWagon = new Lokomotive("ICE",400);
		Personenwagen nachfolger = new Personenwagen ("Metronom",300);
		testWagon.anhaengen(nachfolger);
		assertEquals(testWagon.getNachfolger(), nachfolger);
	}

	@Test
	public void testIstLetzterWagen() 
	{
		Lokomotive testWagon = new Lokomotive("ICE",400);
		assertEquals(testWagon.getNachfolger(), null);
		assertEquals(testWagon.istLetzterWagen(), true);
		
		Personenwagen nachfolger = new Personenwagen ("Metronom",300);
		testWagon.anhaengen(nachfolger);
		assertEquals(testWagon.getNachfolger(), nachfolger);
		assertEquals(testWagon.istLetzterWagen(), false);
	}
}
