package paket1.eisenbahnzug;

/**
 * Exemplare dieser Klasse repr�sentieren Wagons eines Zugs.
 * 
 * @author Julia Stache, Chris Bargmann
 *
 */
abstract class AbstractWagon {
	protected String _bezeichner;
	protected AbstractWagon _nachfolger;

	/**
	 * Initialisiert einen Wagon mit einem Bezeichner.
	 * 
	 * @param bezeichner
	 *            Der Bezeichner des Wagons.
	 * @require !bezeichner.equals("")
	 */

	public AbstractWagon(String bezeichner) {
		assert !bezeichner.equals("") : "Vorbedingung verletzt: String darf nicht leer sein";
		_bezeichner = bezeichner;
	}

	/**
	 * H�ngt einen Wagon hinten an.
	 * 
	 * @param wagon
	 *            Der anzuh�ngende Wagon
	 * @require wagon != null
	 */
	public void anhaengen(AbstractWagon wagon) {
		assert wagon != null : "Vorbedingung verletzt: wagon != null";
		_nachfolger = wagon;
	}

	/**
	 * Pr�ft, ob der Wagen einen Nachfolger hat
	 * 
	 * @return true, wenn es keinen Nachfolger gibt,
	 * @return false, wenn es einen Nachfolger gibt
	 */
	public boolean istLetzterWagen() {
		return _nachfolger == null;
	}

	/**
	 * Liefert den nachfolgenden Wagen.
	 * 
	 * @return Den Nachfolger des aktuellen Wagens.
	 */
	public AbstractWagon getNachfolger() {
		return _nachfolger;
	}

	/**
	 //TODO
	 * Gibt eine Beschreibung aller angeh�ngten Wagen zur�ck.
	 * 
	 * @param wagon
	 * @return Eine Beschreibung aller angeh�ngten Wagen.
	 */
	public String getZugbeschreibungRekursiv()
	{
		
		AbstractWagon wagon = this;
		if (wagon.istLetzterWagen())
		{
			return wagon.toString();
		} 
		else 
		{
			return wagon.toString() + " - " + wagon.getNachfolger().getZugbeschreibungRekursiv();
		}
	}
}
