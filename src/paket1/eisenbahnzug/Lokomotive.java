package paket1.eisenbahnzug;

/**
 * Exemplare dieser Klasse repr�sentieren eine Lokomotive.
 * 
 * @author Julia Stache, Chris Bargmann
 *
 */
public class Lokomotive extends AbstractWagon 
{
	private int _zugkraft;

	/**
	 * Initialisiert eine Lokomotive mit einem Bezeichner und einer Zugkraft.
	 */
	public Lokomotive(String bezeichner, int zugkraft) 
	{
		super(bezeichner);
		_zugkraft = zugkraft;
	}

	/**
	 * Gibt eine Beschreibung der Lokomotive als String zur�ck.
	 */
	@Override
	public String toString() 
	{
		return "Name: " + _bezeichner + ", Zugkraft: " + _zugkraft;
	}



	/**
	 * Gibt eine Beschreibung aller angeh�ngten Wagen zur�ck.
	 * 
	 * @param wagon
	 * @return Eine Beschreibung aller angeh�ngten Wagen.
	 */
	public String getZugbeschreibungIterativ() 
	{
		AbstractWagon aktuellerWagen = this;
		String antwort = aktuellerWagen.toString();
		while (aktuellerWagen.getNachfolger() != null) {
			aktuellerWagen = aktuellerWagen.getNachfolger();
			antwort = antwort + " - " + aktuellerWagen.toString();
		}
		return antwort;

	}

}
