package paket1.eisenbahnzug;

/**
 * Exemplare dieser Klasse repr�sentieren einen Zug. Ein Zug beginnt mit einer
 * Lokomotive, an die weitere Personenwagen oder Lokomotiven angeh�ngt werden
 * k�nnen.
 * 
 * @author Julia Stache, Chris Bargmann
 *
 */
public class Zug 
{
	private Lokomotive _lokomotive;
	private AbstractWagon _letzterWagen;

	/**
	 * Initialisiert einen Zug, der von der �bergebenen Lokomotive gezogen wird.
	 * @param lokomotive  Die f�hrende Lokomotive
	 * @require lokomotive!=null
	 */
	public Zug(Lokomotive lokomotive) 
	{
		assert lokomotive != null : "Vorbedingung verletzt: lokomotive != null";
		_lokomotive = lokomotive;
	}

	/**
	 * H�ngt eine Lokomotive oder einen Personenwagen an.
	 * @require waggon!=null
	 * @param waggon
	 */
	public void haengeAn(AbstractWagon waggon) 
	{
		assert waggon != null : "Vorbedingung verletzt: waggon != null";
		if (_lokomotive.istLetzterWagen()) {
			_lokomotive.anhaengen(waggon);
			_letzterWagen = waggon;
		} else {
			_letzterWagen.anhaengen(waggon);
			_letzterWagen = waggon;
		}
	}

	/**
	 * Gibt einen String der aneinanderh�ngenden Beschreibungen aller Wagons
	 * zur�ck.
	 * 
	 * @return Eine Beschreibung aller Wagons.
	 */
	public String gibZugbeschreibung1Iterativ() 
	{
		if (_lokomotive.istLetzterWagen())
		{
			return _lokomotive.toString();
		}
		AbstractWagon aktuellerWaggon = _lokomotive;
		String antwort = aktuellerWaggon.toString();
		while (aktuellerWaggon.getNachfolger() != (null))
		{
			aktuellerWaggon = aktuellerWaggon.getNachfolger();
			antwort = antwort + " - " + aktuellerWaggon.toString();
		}
		return antwort;
	}
	//TODO
	public String gibZugbeschreibung1Rekursiv()
	{
	    return _lokomotive.getZugbeschreibungRekursiv();
	}

	/**
	 * Gibt einen String der aneinanderh�ngenden Beschreibungen aller Wagons
	 * zur�ck.
	 * 
	 * @return Eine Beschreibung aller Wagons.
	 */
	public String gibZugbeschreibung2Rekursiv()
	{
		return _lokomotive.getZugbeschreibungRekursiv();
	}

	/**
	 * Gibt einen String der aneinanderh�ngenden Beschreibungen aller Wagons
	 * zur�ck.
	 * 
	 * @return Eine Beschreibung aller Wagons.
	 */
	public String gibZugbeschreibung2Iterativ() 
	{
		return _lokomotive.getZugbeschreibungIterativ();
	}

	/**
	 * Gibt die Laenge eines Zuges zur�ck.
	 * @require wagon!=null
	 * @return Anzahl der Wagen des Zugs.
	 */
	public int gibLaenge(AbstractWagon wagon) 
	{

		if (wagon.istLetzterWagen())
		{
			return 1;
		} 
		else
		{
			return 1 + gibLaenge(wagon.getNachfolger());
		}

	}

}
