package paket1.eisenbahnzug;
/**
 * Exemplare dieser Klasse repräsentieren die Personenwagen eines Zuges. 
 * @author Julia Stache, Chris Bargmann 
 *
 */
public class Personenwagen extends AbstractWagon
{
	private int _sitzanzahl;
	
	/**
	 * Initialisiert einen Personenwagen mit Bezeichner und Sitzanzahl.
	 * @param bezeichner 
	 * @param sitzanzahl
	 */
	public Personenwagen(String bezeichner, int sitzanzahl)
	{
		super(bezeichner);
		_sitzanzahl= sitzanzahl;
	}
	/**
	 * Gibt eine Beschreibung des Personenwagens als String zurück.
	 */
	@Override
	public String toString()
	{
		return "Name: " + _bezeichner + ", Sitzanzahl: "+ _sitzanzahl;
	}

	
}
