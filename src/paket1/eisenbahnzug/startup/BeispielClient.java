package paket1.eisenbahnzug.startup;

import paket1.eisenbahnzug.Lokomotive;
import paket1.eisenbahnzug.Personenwagen;
import paket1.eisenbahnzug.Zug;
/**
 * 
 * @author Julia Stache, Chris Bargmann
 * Diese Klasse erstellt einen Beispielzug und gibt seine Beschreibung auf der
 * Konsole aus.
 *
 */
public class BeispielClient 
{

	private static Lokomotive _lokomotive;
	public static void main(String[] args)
	{
		Zug zug = erstelleZug();
		System.out.println(zug.gibZugbeschreibung1Iterativ());
		System.out.println(zug.gibZugbeschreibung1Rekursiv());
		System.out.println(zug.gibZugbeschreibung2Iterativ());
		System.out.println(zug.gibZugbeschreibung2Rekursiv());
		
	}
	/**
	 * Erstellt einen Beispielzug.
	 * @return Ein Zug, der 10 Personenwagen mit unterschiedlicher Sitzanzahl 
	 * und eine Zugkraft von 10 hat.
	 */
	private static Zug erstelleZug()
	{
		// Erstellt eine Beispiellokomotive mit zehn Wagen.
		_lokomotive= new Lokomotive("Roland", 10);
		Zug zug = new Zug(_lokomotive);
		for (int i=1; i<11; i++)
		{
			zug.haengeAn(new Personenwagen("Waggon " + i , i*12));
		}
		return zug;
	}

}
