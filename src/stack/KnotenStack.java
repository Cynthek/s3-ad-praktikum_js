package stack;
/**
 * 
 * @author Julia Stache, Chris Bargmann
 * Exemplare dieser Klasse repr�sentieren einen Stapel, auf den Objekte abgelegt
 * und heruntergenommen werden k�nnen.
 *
 */
public class KnotenStack extends AbstractADStack implements ADStack 
{
	private Knoten _oberster;
	
	public KnotenStack()
	{
		_oberster=null;
	}
	/**
	 * Löscht das zuletzt auf den Stack gelegte Objekt.
	 */
	@Override
	public void popImpl() 
	{
		_oberster= _oberster.gibNachfolger();
		_size++;

	}
	/**
	 * @return Gibt das zuletzt auf den Stack gelegte Objekt zurück.
	 */
	@Override
	public Object topImpl()
	{
		return _oberster.gibInhalt();
	}
	/**
	 * Legt das gegebene Objekt auf den Stack.
	 * @param object Wird auf den Stack gelegt.
	 * @require object !=null
	 */
	@Override
	public void pushImpl(Object object)
	{
		//Wenn der Stack noch leer ist, wird neuer Knoten mit dem übergebenen
		//Objekt erzeugt.
		if (_oberster==null)
		{
			_oberster = new Knoten(object);
		}
		//Sonst wird neuer Knoten erzeugt, dem der oberste Knoten als Nachfolger angehängt wird. Dann wird
		//der neue Knoten oberster.
		else
		{
			Knoten neuerOberster= new Knoten(object);
			neuerOberster.setzeNachfolger(_oberster);
			_oberster=neuerOberster;
		}
		_size++;
	}
	
	/**
	 * Gibt einen String zurück, der für jedes enthaltene Objekt eine Zeile erhält,
	 * beginnend mit dem zuletzt abgelegten Objekt.
	 * @return Ein String, der die abgelegten Objekte repräsentiert.
	 */
	@Override
	public String getStringRepresentationImpl()
	{
		if (_oberster==null)
		{
			return "";
		}
		else
		{
		Knoten temp = _oberster;
		String antwort =temp.toString();
		while (temp.gibNachfolger()!=null)
		{
			temp=temp.gibNachfolger();
			antwort = antwort + "\n" + temp.toString();
		}
		return antwort;
		}
	}

}
