package stack;
/**
 * 
 * @author Julia Stache, Chris Bargmann
 * Exemplare dieser Klasse repr�sentieren einen Knoten, dem ein Inhalt und 
 * ein Nachfolger zugewiesen werden kann.
 *
 */
public class Knoten 
{
	private Knoten _nachfolger;
	private Object _inhalt;
	
	/**
	 * Initialisiert einen Knoten mit dem übergebenen Objekt als Inhalt.
	 * @param object Der Inhalt des Knotens.
	 * @require object!=null
	 */
	public Knoten(Object object)
	{
		assert object != null : "Vorbedingung verletzt: object != null";
		_inhalt= object;
	}
	/**
	 * Der �bergebene Knoten wird Nachfolger des aktuellen Objekts.
	 * @param nachfolger
	 * @require nachfolger!=null
	 */
	public void setzeNachfolger(Knoten nachfolger)
	{
		assert nachfolger != null : "Vorbedingung verletzt: nachfolger != null";
		_nachfolger=nachfolger;
	}
	/**
	 * Gibt den Nachfolgeknoten zur�ck.
	 * @return Der nachfolgende Knoten
	 */
	public Knoten gibNachfolger()
	{
		
		return _nachfolger;
	}
	/**
	 * Gibt eine Beschreibung des Inhalts des Knotens zur�ck.
	 */
	@Override
	public String toString()
	{
		return _inhalt.toString();
	}
	public Object gibInhalt()
	{
		return _inhalt;
	}
	
}
