package stack;

public interface ADStack {
	/**
	 * Legt das gegebene Objekt auf den Stack.
	 * 
	 * @param object
	 *            Wird auf den Stack gelegt.
	 * @require object !=null
	 */
	void push(Object object);

	/**
	 * @return Gibt das zuletzt auf den Stack gelegte Objekt zur�ck.
	 */
	Object top();

	/**
	 * Löscht das zuletzt auf den Stack gelegte Objekt.
	 */
	void pop();

	/**
	 * Gibt einen String zur�ck, der f�r jedes enthaltene Objekt eine Zeile
	 * erh�lt, beginnend mit dem zuletzt abgelegten Objekt.
	 * 
	 * @return Ein String, der die abgelegten Objekte repr��sentiert.
	 */
	String getStringRepresentation();

}
