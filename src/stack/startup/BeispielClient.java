package stack.startup;

import stack.ArrayADStack;
import stack.KnotenStack;
/**
 * 
 * @author Julia Stache, Chris Bargmann
 * Diese Klasse erstellt zwei Beispielstacks, bef�llt sie und gibt ihren Inhalt
 * auf der Konsole aus.
 *
 */
public class BeispielClient 
{
	
	public static void main(String[] args)
	{
		ArrayADStack arrayStack= erstelleArrayStack();
		KnotenStack knotenStack= erstelleKnotenStack();
		System.out.println(arrayStack.getStringRepresentation());
		System.out.println(knotenStack.getStringRepresentation());
		
	}
	private static ArrayADStack erstelleArrayStack()
	{
		ArrayADStack arrayStack = new ArrayADStack();
		arrayStack.push("eins");
		arrayStack.push("zwei");
		arrayStack.push("drei");
		arrayStack.push("vier");
		return arrayStack;
	}
	private static KnotenStack erstelleKnotenStack()
	{
		KnotenStack knoten = new KnotenStack();
		knoten.push("F�NF");
		knoten.push("SECHS");
		knoten.push("SIEBEN");
		return knoten;
		
	}
}
