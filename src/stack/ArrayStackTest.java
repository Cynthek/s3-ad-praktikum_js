package stack;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
/**
 * 
 * @author Julia Stache, Chris Bargmann
 *
 */
public class ArrayStackTest
{
	protected AbstractADStack stack;
	

	/**
	 * Testet, ob Objekte auf dem Stack abgelegt werden. 
	 */
	@Test
	public void testPushUndTop()
	{
		ArrayADStack stack = new ArrayADStack();
		stack.push("Eins");
		assertTrue(stack.top().equals("Eins"));
		stack.push("Zwei");
		assertTrue(stack.top().equals("Zwei"));
		stack.push("Drei");
		assertTrue(stack.top().equals("Drei"));
	}
	/** 
	 * Testet, ob das zuletzt auf den Stapel gelegte Objekt zur�ckgegeben und gel�scht  wird. 
	 */
	@Test
	public void testPop()
	{
		ArrayADStack stack = new ArrayADStack();
		stack.push("Eins");
		stack.push("Zwei");
		stack.push("Drei");
		stack.pop();
		assertTrue(stack.top().equals("Zwei"));
		stack.pop();
		assertTrue(stack.top().equals("Eins"));
		
	}
	@Test
	public void testTop()
	{
		ArrayADStack stack = new ArrayADStack();
		stack.push("Eins");
		assertTrue(stack.top().equals("Eins"));
		stack.push("Zwei");
		assertTrue(stack.top().equals("Zwei"));
		stack.push("Drei");
		assertTrue(stack.top().equals("Drei"));
		
	}



}
