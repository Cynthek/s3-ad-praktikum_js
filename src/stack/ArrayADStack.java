package stack;
/**
 * 
 * @author Julia Stache, Chris Bargmann
 * Exemplare dieser Klasse repr�sentieren einen Stapel, auf den Objekte abgelegt
 * und heruntergenommen werden k�nnen.
 *
 */
public class ArrayADStack extends AbstractADStack implements ADStack 
{

	private Object[] _arrayStack;
	private int _naechsterFreierIndex;
	private int _arraygroesse;
	
	public ArrayADStack()
	{
		_arrayStack= new Object[10];
		_arraygroesse=10;
		_size=0;
		_naechsterFreierIndex=0;
	}
	@Override
	/**
	 * L�scht das oberste Objekt
	 */
	public void popImpl()
	{
		_arrayStack[_naechsterFreierIndex-1] = null;
		_naechsterFreierIndex= _naechsterFreierIndex-1;
		_size=_size-1;

	}
	/**
	 * Gibt das letzte Objekt zur�ck, ohne es zu entfernen.
	 */
	@Override
	public Object topImpl()
	{
		
		return _arrayStack[_naechsterFreierIndex-1];
	}

	@Override
	/**
	 * Legt ein Objekt auf dem Stack ab. 
	 */
	public void pushImpl(Object object) 
	{
		_arrayStack[_naechsterFreierIndex]= object;
		_naechsterFreierIndex= _naechsterFreierIndex + 1;
		if (_naechsterFreierIndex == _size)
		{
			stackVerdoppeln();
		}
		_size=_size+1;
	}
	private void stackVerdoppeln()
	{
		_arraygroesse= _arraygroesse*2;
		_arrayStack= new Object[_size];
	}
	
	@Override
	public String getStringRepresentationImpl() 
	{
		String antwort="";
		for (int i= _naechsterFreierIndex-1; i >= 0; i--)
		{
			antwort= antwort + "\n" + _arrayStack[i];
		}
		return antwort;
	}

}
