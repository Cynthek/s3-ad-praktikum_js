package stack;
/**
 * 
 * @author Julia Stache, Chris Bargmann
 * Exemplare dieser Klasse repr�sentieren einen Stapel, auf den Objekte abgelegt
 * und heruntergenommen werden k�nnen.
 *
 */
public abstract class AbstractADStack implements ADStack
{
	protected int _size;
	
	/**
	 * Legt das gegebene Objekt auf den Stack.
	 * @param object Wird auf den Stack gelegt.
	 * @require object !=null
	 */
	public void push(Object object)
	{
		assert object != null : "Vorbedingung verletzt: object != null";
		pushImpl(object);
		//Nachbedingungen werden doch mit Test geprüft??
	}

	
	/**
	 * @return Gibt das zuletzt auf den Stack gelegte Objekt zurück.
	 * @require  getSize()>0
	 */
	public  Object top()
	{
		assert getSize()>0 : "Vorbedingung verletzt: Stack ist leer";
		return topImpl();
	}

	
	/**
	 * L�scht das zuletzt auf den Stack gelegte Objekt.
	 * @require getSize()>0
	 */
	public void pop()
	{
		assert getSize()>0 : "Vorbedingung verletzt: Stack ist leer";
		popImpl();
	}
	
	/**
	 * Gibt einen String zurück, der für jedes enthaltene Objekt eine Zeile erhält,
	 * beginnend mit dem zuletzt abgelegten Objekt.
	 * @return Ein String, der die abgelegten Objekte repräsentiert.
	 */
	public String getStringRepresentation()
	{
		return getStringRepresentationImpl();
	}
	public int getSize()
	{
		return _size;
	}
	protected abstract void popImpl();
	protected abstract Object topImpl();
	protected abstract void pushImpl(Object object);
	protected abstract String getStringRepresentationImpl();

}
