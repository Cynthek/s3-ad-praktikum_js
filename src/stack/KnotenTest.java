package stack;

import static org.junit.Assert.*;

import org.junit.Test;
/**
 * 
 * @author Julia Stache, Chris Bargmann
 *
 */
public class KnotenTest
{

	@Test
	public void testeNachfolger()
	{
		Knoten testknoten = new Knoten("Inhalt");
		Knoten testnachfolger= new Knoten ("Nachfolger");
		testknoten.setzeNachfolger(testnachfolger);
		assertEquals(testknoten.gibNachfolger(), testnachfolger);
	}
	
	@Test
	public void testeGibInhalt()
	{
		Knoten testknoten = new Knoten("Inhalt");
		assertEquals(testknoten.gibInhalt(), "Inhalt");
	}
}
